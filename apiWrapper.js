var request = require('request');
var fs = require('fs');



/************************************************* 
 *              MODULE API_WRAPPER               *
*************************************************/


module.exports = function() {
    key = "ae7b673aaba648fdb99e292b6de24c50"
    var personGroupId = "cherry";
    var uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0";
    var apiWrapper = {};

    /**
     * @param imageID - path or URI of an jpeg image
     * @param callback - function to process the result
     */
    apiWrapper.detect = function(imageID, callback){
                                    var rePattern = new RegExp(/^http/);
                                    var arrMatches = imageID.match(rePattern);
                                    if(arrMatches != null){
                                        detect_face(imageID, key, uriBase, callback);
                                    }
                                    else{
                                        detect_face_local(imageID, key, uriBase, callback);
                                    }
                                }

    /**
     * @description create a person in order into a personGroup (cherry by default)
     * @param name - name of the person to create
     * @param callback - function to process the result
     */
    apiWrapper.create_person = function(name, callback){
                                        create_person(personGroupId, name, uriBase, callback);
                                    }

    /**
     * @param name - name of the person to create
     * @param callback - function to process the result
     */
    apiWrapper.delete_person = function(personId, callback){
                                        delete_person(personGroupId, personId, uriBase, callback);
                                    }

    /**
     * @description add a face to a person to be able to train the personGroup
     * @param imageID - path or URI of an jpeg image
     * @param personID - id of a person previously created -> refer to create_person(name, callback)
     * @param callback - function to process the result
     */
    apiWrapper.add_person_face = function(imageID, personId, callback){
                                        var rePattern = new RegExp(/^http/);
                                        var arrMatches = imageID.match(rePattern);
                                        if(arrMatches != null){
                                            add_person_face(imageID, personGroupId, personId, uriBase, callback);
                                        }
                                        else{
                                            add_person_face_local(imageID, personGroupId, personId, uriBase, callback);
                                        }
                                    };

    /**
     * @description see if the imageID can be associated to an existing person into a personGoup (cherry by default)
     * @param imageID - path or URI of an jpeg image
     * @param personID - id of a person already created -> refer to create_person(name, callback)
     * @param callback - function to process the result
     */                                    
    apiWrapper.recognize = function(imageID,callback){
                                    this.detect(imageID, function(response){
                                        if(response != undefined){
                                            //var faceId = JSON.parse(response)[0]["faceId"];
                                            var obj = JSON.parse(response);
                                            var facesId = [] ;
                                            for(var t = 0 ; t < obj.length; t++) {
                                                facesId.push(obj[t]["faceId"]);
                                            }
                                            recognize_face(facesId, personGroupId, uriBase, obj, callback);
                                        }
                                    });
                                }

    /**
     * @description train a person group to be able to recognize faces into it
     * @description always train a personGroup after added a new face or a new person
     */                                    
    apiWrapper.train_person_group = function(callback){
                                train_person_group(personGroupId, uriBase, callback);
                            }

    /**
     * @description train a person group to be able to recognize faces into it
     * @description always train a personGroup after added a new face or a new person
     */                                    
    apiWrapper.list_persons_group = function(callback){
                                list_persons_group("cherry", uriBase, callback);
                            }   
                                
    return apiWrapper;
}();



/************************************************* 
 *                 FUNCTIONS                     *
*************************************************/

function train_person_group(personGroupId, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/persongroups/" + personGroupId + "/train";
    var body = "";
    var params = {};
    var method = "POST";
    _request(params, content_type, method, body, uri, callback);
}

function recognize_face(faceId, personGroupId, uriBase, obj, callback){
    var content_type = "application/json";
    var uri = uriBase + "/identify";
    var body = '{"personGroupId":"' + personGroupId + '", "faceIds":' + JSON.stringify(faceId) + '}';
    var params = {};
    var passToCallback = {"detection" : obj }
    var method = "POST";
    _request(params, content_type, method, body, uri, callback, passToCallback);
}

function add_person_face(imageURI, personGroupId, personId, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/persongroups/" + personGroupId + "/persons/" + personId + "/persistedFaces";
    var body = '{"url":"' + imageURI + '"}';
    params = {}
    var method = "POST";
    _request(params, content_type, method, body, uri, callback);
}

function add_person_face_local(imagePath, personGroupId, personId, uriBase, callback){
    fs.readFile(imagePath, function(err, data) {
        var content_type = "application/octet-stream";
        var uri = uriBase + "/persongroups/" + personGroupId + "/persons/" + personId + "/persistedFaces";
        var body = data;
        params = {}
        var method = "POST";
        _request(params, content_type, method, body, uri, callback);
    });
}

function create_person(personGroupId, name, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/persongroups/" + personGroupId + "/persons";
    var body = '{"name": ' + '"' + name + '"}';
    var params = {}
    var method = "POST";
    _request(params, content_type, method, body, uri, callback);
}

function delete_person(personGroupId, personId, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/persongroups/" + personGroupId + "/persons/" + personId;
    var body = "{}";
    var params = {};
    var method = "DELETE";
    _request(params, content_type, method, body, uri, callback);
}

function detect_face(imageURI, key, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/detect";
    var params = {
        "returnFaceId": "true",
        "returnFaceLandmarks": "false",
        "returnFaceAttributes": "age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"
    };
    var body = '{"url": ' + '"' + imageURI + '"}';
    var method = "POST";
    _request(params, content_type, method, body, uri, callback);
}


function detect_face_local(imagePath, key, uriBase, callback){
    fs.readFile(imagePath, function(err, data) {
        var content_type = "application/octet-stream";
        var uri = uriBase + "/detect";
        var params = {
            "returnFaceId": "true",
            "returnFaceLandmarks": "false",
            "returnFaceAttributes": "age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"
        };
        var body = data;
        var method = "POST";
        _request(params, content_type, method, body, uri, callback);
    });
}

function list_persons_group(personGroup, uriBase, callback){
    var content_type = "application/json";
    var uri = uriBase + "/persongroups/" + personGroup + "/persons";
    var params = {};
    var body = "";
    var method = "GET";
    _request(params, content_type, method, body, uri, callback);
}



/************************************************* 
 *                 REQUESTS                      *
*************************************************/

function _request(params, content_type, method, body, uri, callback, passToCallback){
    var p = param(params);
    if(p != ""){
        uri = uri + "?" + p;
    }
    
    var header = 
        {
            "headers": {
                'Content-Type': content_type,
                'Ocp-Apim-Subscription-Key': key
            },
            "url": uri,
            "body": body
        }

    var body = function(err, response, body){
            if(body != ""){
                var parsedBody = JSON.parse(body);

               if (parsedBody.success === false){
                    callback(response, passToCallback);
                }
                else{
                    callback(response.body, passToCallback);
                }
            }
            else{
                console.log("Pas de réponse de la requête");
            }
        }

    if     (method == "POST") request.post(header, body);
    else if(method == "DELETE") request.delete(header, body);
    else if(method == "GET") request.get(header, body);
}



/************************************************* 
 *                   UTILS                       *
*************************************************/

function param(option){
    str = ""
    if(Object.keys(option).length > 0){
        for(var i = 0; i < Object.keys(option).length - 1; i++){
            str += Object.keys(option)[i] + "=" + Object.values(option)[i] + "&";
        }
        str += Object.keys(option)[i] + "=" + Object.values(option)[Object.keys(option).length - 1];
    }
    return str
}
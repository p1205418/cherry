$(function(){

    var table = $("tbody");

    table.on("click", "#onoff", function(){
        var that = $(this);
        var status = that.attr("onOff");
        var device = that.attr("device");
        var ip = that.attr("ip");
        if(status=="true"){
            $.ajax({
                url : "/turnOff",
                method : "POST",
                data : {
                    "device" : device
                }
            }).done(function(data, status, xhr){
                that.attr("onOff","false");
                that.text("Allumer");
            }).fail(function(xhr, status, err){
                console.log("fail");
            })
        }else{
            $.ajax({
                url : "/turnOn",
                method : "POST",
                data : {
                    "device" : device,
                    "ip" : ip
                }
            }).done(function(data, status, xhr){
                that.attr("onOff","true");
                that.text("Eteindre");
            }).fail(function(xhr, status, err){
                console.log("fail");
            })
        }
    });

    table.on("click", "#mode", function(){
        var that = $(this);
        var status = that.attr("mode");
        var device = that.attr("device");
        var mode = status == "A" ? "M" : "A" ;
        $.ajax({
            url : "/changeMode",
            method : "POST",
            data : {
                "device" : device,
                "mode" : mode
            }
        }).done(function(data, status, xhr){
            that.attr("mode",mode);
            that.text(mode);
        }).fail(function(xhr, status, err){
            console.log("fail motor");
        })
    });


});
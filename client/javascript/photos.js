/**
 * Created by aubret on 05/12/17.
 */
$(function(){

    $("#list_devices").on("click", "a", function(){
        var that = $(this);
        var device=that.attr("device");
        $(".list_photos").css("display","none");
        $("#dev_"+device).css("display","inline");
    });
})
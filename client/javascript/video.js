/**
 * Created by aubret on 18/12/17.
 */
$(function () {
    var video = $("#onevideo");
    video.removeClass("selectMove");
    $(video[0]).addClass("selectMove");


    _actualIp = undefined;
    $("body").on("click", "#onevideo", function () {
        var that = $(this);
        $("#onevideo").removeClass("selectMove");
        that.addClass("selectMove");

    });

    $("table").on("click", "#moveLeft", function () {
        var that = $(this);
        var video = that.closest("#onevideo");
        var ip = video.attr("ipmotor");
        move(ip, 50);
    });

    $("table").on("click", "#moveRight", function () {
        var that = $(this);
        var video = that.closest("#onevideo");
        var ip = video.attr("ipmotor");
        move(ip, -50);

    });

    move = function (ip, intensity) {
        console.log(ip);
        $.ajax({
            type: "POST",
            url: ip + "?move=" + intensity,
            contentType: "text",
            processData: false
        });

    };

    window.onkeyup = function (e) {
        var ip = $(".selectMove").attr("ipmotor");
        var key = e.keyCode ? e.keyCode : e.which;
        if (key === 37) {
            move(ip, 25)
        } else if (key === 39) {
            move(ip, -25)
        }
        /*else if (key===32) {
                    mod()
                }*/
    }
});
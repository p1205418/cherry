var faces = require("./faces");
var apiWrapper = require("./apiWrapper");

/************************************************* 
 *            MODULE FACE_RECOGNITION            *
*************************************************/


module.exports = function() {
    db_faces = faces.get_persons();
    var faceRecognition = {};

    /**
     * @description Cherche si la personne présente sur l'image est une personne présente dans la base de connaissance
     * @param imageID - path or URI of an jpeg image
     * @param callback - function to process the result
     * @return JSON [{name, confidence}]
     */
    faceRecognition.recognize = function(imageID, callback){
        apiWrapper.recognize(imageID, function(response, params){
            var json = JSON.parse(response);
            var res = [];
            if(json.error != undefined)
                return res;
            for(var t = 0 ; t < json.length ; t++) {
                if (Object.keys(json[t]["candidates"]).length == 0) {
                    //detect(imageID, callback);
                    params.detection[t].found = false ;
                    res.push(params.detection[t]);
                } else {
                    var obj = { found : true, faceRectangle : params.detection[t].faceRectangle };
                    var array = json[t]["candidates"];
                    var db_size = Object.keys(db_faces).length;
                    var size = Object.keys(array).length;
                    //Creation d'un json contenant les candidats en fonction de leur nom et leur confiance
                    //(au lieu de leur personId)
                    for (var i = 0; i < size; i++) {
                        var name = "";
                        var id = array[i]["personId"];
                        for (var j = 0; j < db_size; j++) {
                            if (id == db_faces[j]["id"]) {
                                name = db_faces[j]["name"];
                                var confidence = array[i]["confidence"];
                                obj.candidates = {"name": name, "confidence": confidence};
                                break;
                            }
                        }
                    }
                    res.push(obj);
                }
            }
            callback(res);
        });
    };
                                
    return faceRecognition;
}();

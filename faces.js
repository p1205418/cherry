module.exports = function() {
    var faces = {};
    var db_persons = [
                    {"name": "Mickael", "id": "84e7d62c-111c-44be-9270-4942c37ee74f", 
                            "faces": [{"id": "20ac3865-8dbd-4941-bf8c-71058548af9d"}, 
                                      {"id": "1c00ffd9-0675-46be-9c83-c49926d979fd"},
                                      {"id": "21d42b1f-f9fd-4bb3-9f13-e4fa7560b811"},
                                      {"id": "bbb84242-27f4-480e-a639-411cb0f50c6d"},
                                      {"id": "8e671a33-7e63-4268-818d-cec431cfa3a9"},
                                      {"id": "d5b8fe66-6e50-4d0a-82c1-a8d5847c571f"}
                            ]},
                    {"name": "Miguel", "id": "bda1d6a7-808d-44cb-9553-a79e7379edc1", "faces": [{"id": ""}]}, 
                    {"name": "Arthur", "id": "a9106273-968f-4a4d-8031-ce390a2d4d11",
                            "faces": [{"id": "921e0576-a37d-4856-9405-eddd385274e4"},
                                      {"id": "fe8fa64b-13c7-4141-937d-1faa68a9529e"},
                                      {"id": "aabba89c-5c44-4c63-8b9a-9b2ba6d18765"},
                                      {"id": "27cd2d31-57ab-4692-862d-bee816c89db5"},
                                      {"id": "609b62b8-4ff8-458b-9016-8e18ad3a7c5b"},
                                      {"id": "993be2ce-881b-48ab-8e4a-c11436665d66"},
                                      {"id": "3d959594-e97d-4172-90d1-50dc8a5b3f1b"}
                            ]}, 
                    {"name": "Theo", "id": "4ff1be11-a00a-4c80-9d90-d1fd2b21bf37", 
                            "faces": [{"id": "625eb635-47c9-4fdd-8a9a-56b24403609c"},
                                      {"id": "e603b002-a330-4c39-8ee5-d093674e5a3c"},
                                      {"id": "72e9e415-b699-4032-9bc5-1bbb62926ed2"},
                                      {"id": "c5fe6078-4ce3-40f1-96c0-4961097c0a95"},
                                      {"id": "b2258db2-48ab-42be-9d56-8cdc5ca250d3"},
                                      {"id": "7173c44e-43d8-4c5d-9219-da450e5a0898"}
                            ]}
                    ];

    faces.get_persons = function(name){
        return db_persons;
    }

    return faces;
}();
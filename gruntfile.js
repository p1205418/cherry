/**
 * Created by Nathan on 04/04/2017.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        express: {
            build: {
                options : {
                    server: ('server.js'),
                    port: 8080,
                    hostname:"localhost"
                }
            }
        },
        jshint: {
            files: ['*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-express');
    grunt.registerTask('build', ['express', 'express-keepalive']);

};
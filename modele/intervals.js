/**
 * Created by aubret on 05/12/17.
 */
var Intervals = function(){
    this.collection = {};
};

Intervals.prototype.addIP=function(name,device,interval){
    this.collection[name+"_"+device] = interval ;
};

Intervals.prototype.removeIP=function(name,device){
    delete this.collection[name+"_"+device];
};


Intervals.prototype.getId=function(name,device){
    return this.collection[name+"_"+device];
};

var intervals = new Intervals() ;
module.exports = function(){ return intervals};
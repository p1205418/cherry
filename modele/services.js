/**
 * Created by aubret on 17/12/17.
 */
var Jimp = require("jimp");
var urllib  = require("urllib") ;


var services = function(){};

services.prototype.follow = function(img, position, ipmotor){
    var image = new Jimp(img, function (err, image) {
        var wtotal = image.bitmap.width; // the width of the image
        var center = position.x + position.width / 2 ;
        var totalCenter = wtotal / 2 ;
        var decalage = center-totalCenter ; // POsitif à droite, négatif à gauche
        var realMove = -750 * decalage * position.width/  ( wtotal * wtotal / 2 );
        urllib.request(ipmotor+ "?move="+realMove);
    });
};

services.prototype.writeName=function(img, position, name){
    //var img = "../"+img ;
    Jimp.read(img, function (err, image) {
        Jimp.loadFont( Jimp.FONT_SANS_32_WHITE).then(function (font) { // load font from .fnt file
            image.print(font, position.left, position.top - 80, name)
                .write(img);        // print a message on an image
        });
    });
};


module.exports = new services();
/**
 * Created by aubret on 04/12/17.
 */
var fs = require("fs");
var intervals = require("./intervals")();

var Users = function(){
    var data = fs.readFileSync("./modele/saves.json","utf8");
    this.collection = JSON.parse(data);
    for(var us in this.collection){
        for(var dev in this.collection[us].devices){
            this.collection[us].devices[dev].light=false ;
            this.collection[us].devices[dev].inUse=false ;
        }
    }
};

Users.prototype.create=function(name){
    if(!this.collection[name]) {
        this.collection[name] = {"devices" : {},
            "detections" :
                {
                    "connues": []
                    , "inconnues" : []
                }
        };
    }
};

Users.prototype.email=function(name, email){
    if(!this.collection[name])
        return ;
    this.collection[name].email = email ;
};

Users.prototype.getEmail=function(name){
    if(!this.collection[name])
        return ;
    return this.collection[name].email ;
};

Users.prototype.save=function(){
    var json = JSON.stringify(this.collection); //convert it back to json
    fs.writeFile('./modele/saves.json', json, 'utf8'); // write it back
};

Users.prototype.configureIP = function(name,device,ip,ipvideo,ipmoteur, mode,id){
    if(!this.collection[name].devices[device])
        this.collection[name].devices[device] ={ "photos" : {}, "count" : 0};
    this.collection[name].devices[device].light=true ;
    this.collection[name].devices[device].ip=ip ;
    this.collection[name].devices[device].ipvideo=ipvideo ;
    this.collection[name].devices[device].ipmotor={"ip" : ipmoteur, "mode" : mode };
    this.collection[name].devices[device].inUse = false ;
    intervals.addIP(name,device,id);
};

Users.prototype.unconfigureIP =function(name,device){
    this.collection[name].devices[device].light = false;
    intervals.removeIP(name,device);
};

Users.prototype.intervalId = function(name, device){
    return intervals.getId(name,device);
};

Users.prototype.getIP = function(name){
    return this.collection[name].devices ;
};

Users.prototype.removeIP = function(name, device){
    delete this.collection[name].devices[device];
};

Users.prototype.addPhoto = function(name,device, found){
    var addr = this.collection[name].devices[device];
    var date = (new Date()).toString();
    var obj ={};
    obj.date = date ;
    obj.way = addr.count+"-"+name+"-"+device;
    obj.faces = found ;
    this.collection[name].devices[device].photos[addr.count] = obj ;
    this.save();
    addr.count++ ;
    if(addr.count > 10)
        addr.count=0 ;
    return obj.way ;
};


Users.prototype.putDetection=function(name,detection){
    if(detection.found == false )
        this.collection[name].detections["inconnues"].push({"attributes": detection.faceAttributes, "date": (new Date()).toString()});
    else
        this.collection[name].detections["connues"].push({"name": detection.candidates["name"], "date": (new Date()).toString()})
};

Users.prototype.getDetectionConnue=function(name){
    if(!this.collection[name])
        return {};
    return this.collection[name].detections["connues"];
};

Users.prototype.getDetectionInconnue=function(name){
    if(!this.collection[name])
        return {};
    return this.collection[name].detections["inconnues"];
};

Users.prototype.isAuto = function(name, device){
    return this.collection[name].devices[device].ipmotor.mode == "A" ;
};

Users.prototype.IPMotor = function(name,device){
    return this.collection[name].devices[device].ipmotor.ip ;
};

Users.prototype.stopUse=function(name,device){
    this.collection[name].devices[device].inUse = false ;
};


Users.prototype.startUse=function(name,device){
    this.collection[name].devices[device].inUse = true ;
};

Users.prototype.inUse=function(name,device){
    return this.collection[name].devices[device].inUse;
};

module.exports = function(){ return new Users() } ;
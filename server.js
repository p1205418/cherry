/***************** REQUIRE POUR APP *********************/

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require("fs");
var urllib  = require("urllib") ;
var cv = require('opencv');
var cons = require('consolidate');
var users = require("./modele/user.js")();
var session = require('express-session');
var faceRecognition = require("./faceRecognition");
var mail = require("./modele/transport.js");
var services = require("./modele/services.js");

var app = module.exports = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(path.resolve('node_modules')));
app.use(express.static(path.resolve('client/css'))) ;
app.use(express.static(path.resolve('modele'))) ;
app.use(express.static(path.resolve('client/javascript'))) ;
app.use(express.static(path.resolve('./resources'))) ;
app.use(session({"secret" : "supersecret"}));
app.engine('html', cons.underscore);
app.set('views', __dirname+'/client/html');
app.set('view engine', 'html');

app.use("*", function(req,res,next) {//Authentication
    if (req.baseUrl === '/' || req.baseUrl === '/home' || req.baseUrl === "")
        return next();
    if (!req.session.username) {
        res.redirect("/");
        return ;
    }
    next();

});

app.post('/home', function(req,res) {
    req.session.username = req.body.connect;
    users.create(req.session.username);
    users.save();
    res.render('index',{"name" : req.session.username});
});

app.get('/home', function(req,res) {
    res.render('index',{"name" : req.session.username});
});

app.get('', function(req, res) {
    res.render('index',{"name" : req.session.username});
});

app.get('/photos', function(req, res) {
    var obj = users.getIP(req.session.username);
    res.render('photos', {"ips" : obj,"name" : req.session.username});
});

app.get('/gestion', function(req, res) {
    var obj = users.getIP(req.session.username);
    res.render('gestionIp', {"ips" : obj,"name" : req.session.username, "email" : users.getEmail(req.session.username)});
});

app.post('/gestion', function(req, res) {
    if(req.body.email == undefined) {
        var intervalId = setInterval(detectFaces, 3000, req.body.ip, req.session.username, req.body.name);
        users.configureIP(
            req.session.username,
            req.body.name,
            req.body.ip,
            req.body.ipvideo,
            req.body.ipmoteur,
            "A",
            intervalId
        );
    }else{
        users.email(req.session.username, req.body.email) ;
    }
    users.save();
    res.render('gestionIp', {"ips" : users.getIP(req.session.username),"name" : req.session.username, "email": users.getEmail(req.session.username)});
});

app.get("/video", function(req,res){
    var obj = users.getIP(req.session.username);
    res.render("videoflux", {"ips" : obj,"name" : req.session.username })     ;
});

app.post("/turnOff", function(req, res){
    clearInterval(users.intervalId(req.session.username, req.body.device));
    users.unconfigureIP(req.session.username , req.body.device);
    users.save();
    res.sendStatus(200);
});

app.post("/turnOn", function(req, res){
    var intervalId = setInterval( detectFaces,3000,req.body.ip,req.session.username, req.body.device) ;
    users.configureIP(req.session.username ,
        req.body.device,
        req.body.ip,
        users.getIP(req.session.username)[req.body.device].ipvideo,
        users.getIP(req.session.username)[req.body.device].ipmotor.ip,
        users.getIP(req.session.username)[req.body.device].ipmotor.mode,
        intervalId);
    users.save();
    res.sendStatus(200);
});

app.post("/changeMode", function(req, res){
    var ip =users.IPMotor(req.session.username, req.body.device);
    if( ip != undefined ) {
        var mode = users.isAuto(req.session.username, req.body.device) ? "autom" : "manual";
        urllib.request(ip + "?mode="+mode);
    }
    users.configureIP(
        req.session.username ,
        req.body.device,
        users.getIP(req.session.username)[req.body.device].ip,
        users.getIP(req.session.username)[req.body.device].ipvideo,
        users.getIP(req.session.username)[req.body.device].ipmotor.ip,
        req.body.mode,
        users.intervalId(req.session.username, req.body.device)
    );
    users.save();
    res.sendStatus(200);
});

app.get("/detectionsConnues/:name",function(req,res){
    res.send(users.getDetectionConnue(req.params.name));
});

app.get("/detectionsInconnues/:name",function(req,res){
    res.send(users.getDetectionInconnue(req.params.name));
});

app.get("/test",function(req,res){

    users.startUse(req.session.username, "smartphone");
    follow(
        "https://media3.woopic.com/api/v1/images/165%2FLDQSM%2Fexclu-video-paga-et-adixia-question-salaire-on-est-loin-de-ce-que-peut-gagner-une-personne-des-anges%7Cx240-kl4.jpg?facedetect=1&quality=85",
        "jean",
        "smartphone",
        20
    );
    res.sendStatus(200);
});

var detectFollow=function(adress,user,device,number, person, data){
    if (person != undefined) {
        data.ellipse(person.x + person.width/2, person.y + person.height/2, person.width/2, person.height/2);
        buf=data.toBuffer() ;
        services.follow(buf, person, users.IPMotor(user,device)); // On suit
        setTimeout(function () {
            follow(adress, user, device, number)
        }, 500);
    }else if (number > 0) {
        setTimeout(function () {
            follow(adress, user, device, number)
        }, 500);
    } else {
        console.log("stop iteration");
        users.stopUse(user,device)
    }
};

var follow = function(adress,user,device,number){
    urllib.request(adress).then( function (result) {
        cv.readImage(result.data, function(err, im) {
            var count = 0;
            im.detectObject(cv.FULLBODY_CASCADE, {"neighbors": 5, "scale": 1.2}, function (err, faces) {
                if (faces.length > 0) {
                    console.log("fullbody");
                    detectFollow(adress, user, device, number-1, faces[0],im) ;
                }else{
                    im.detectObject(cv.FACE_CASCADE, {"neighbors": 5, "scale": 1.2}, function (err, faces) {
                        if (faces.length > 0) {
                            console.log("face");
                            detectFollow(adress, user, device, number-1, faces[0],im) ;
                        }else {
                            im.detectObject(cv.FACE_CASCADE_PROFILE, {"neighbors": 5, "scale": 1.1}, function (err, faces) {
                                if(faces.length > 0) {
                                    console.log("profile");
                                    detectFollow(adress, user, device, number-1,faces[0],im) ;
                                }else {
                                    detectFollow(adress, user, device, number - 1);
                                }
                            });
                        }
                    });
                }

            });

        });
    });
};
/*
 , FACE_CASCADE_TREE: 'haarcascade_frontalface_alt_tree.xml'
 , FACE_CASCADE_DEFAULT: 'haarcascade_frontalface_default.xml'
 */

var detectFaces = function(adress,user,device){
    urllib.request(adress).then( function (result) {
        cv.readImage(result.data, function(err, im){
            //var typeDetection = cv.FACE_CASCADE_DEFAULT ;
            //var typeDetection = cv.FACE_CASCADE2 ;
            var typeDetection = cv.FACE_CASCADE ;
            //var typeDetection = cv.FULLBODY_CASCADE ;
            im.detectObject(typeDetection, { "neighbors" : 5,"scale" : 1.2}, function(err, faces){
                for (var i=0;i<faces.length; i++){
                    var x = faces[i] ;
                    im.ellipse(x.x + x.width/2, x.y + x.height/2, x.width/2, x.height/2);
                }
                if(faces.length  > 0){
                     var way = users.addPhoto(user, device,faces.length) ;
                     var picture = "resources/"+way+".jpg";
                     var buffer = im.toBuffer();
                     fs.writeFile(picture, buffer, 'binary', function(err){
                        if (err) throw err ;
                        console.log('File saved. '+way);
                        faceRecognition.recognize(picture, function(response){
                            //console.log("[server.js] " + JSON.stringify(response));
                            var found = false ;
                            for(var i in response){
                                found = found || response[i].found;
                                users.putDetection(user,response[i]);
                                if(response[i].found)
                                    services.writeName(picture, response[i].faceRectangle, response[i].candidates["name"] );
                                else
                                    services.writeName(picture, response[i].faceRectangle, "Intrus" );
                            }
                            if(response.length > 0 && !found) { // Il y a que des inconnus !
                                console.log("INCONNUS !!!!!");
                                var options = mail.alarm(user, users.getEmail(user),picture);
                                mail.send(options);
                                if(!users.inUse(user,device) && users.isAuto(user,device)){
                                    users.startUse(user, device);
                                    console.log("someone detect");
                                    services.follow(picture, faces[0], users.IPMotor(user, device));
                                    setTimeout(function () {
                                        follow(adress, user, device, 10)
                                    }, 500);
                                }
                            }


                        });
                    });
                }else{
                    //console.log("recognition failed");
                }
            });
        });
    }).catch(function (err) {
        console.error("err");
    });
};
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";